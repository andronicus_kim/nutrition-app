package com.example.data

import com.example.domain.DataSource
import com.example.domain.Repository
import com.example.domain.ResourceWrapper
import com.example.domain.models.FoodItem
import com.example.domain.models.Meal
import javax.inject.Inject

/**
 * Created by Andronicus Kim on 10/1/21.
 */
class RepositoryImpl @Inject constructor(private val dataSource: DataSource): Repository {

    override suspend fun addMeal(
        meal: Meal,
        onSuccess: (meal: Meal) -> Unit,
        onError: (message: String) -> Unit
    ) {
        when (val resource = dataSource.addMeal(meal)){
            is ResourceWrapper.OnSuccess -> onSuccess(resource.data!!)
            is ResourceWrapper.OnError -> onError(resource.message!!)
        }
    }

    override suspend fun getMeal(
        id: Int,
        onSuccess: (meal: Meal) -> Unit,
        onError: (message: String) -> Unit
    ) {
        when(val resource = dataSource.getMeal(id)){
            is ResourceWrapper.OnSuccess -> onSuccess(resource.data!!)
            is ResourceWrapper.OnError -> onError(resource.message!!)
        }
    }

    override suspend fun getMeals(
        date: String,
        onSuccess: (meals: List<Meal>) -> Unit,
        onError: (message: String) -> Unit
    ) {
        when(val resource = dataSource.getMeals(date)){
            is ResourceWrapper.OnSuccess -> onSuccess(resource.data!!)
            is ResourceWrapper.OnError -> onError(resource.message!!)
        }
    }

    override suspend fun updateMeal(
        meal: Meal,
        onSuccess: (meal: Meal) -> Unit,
        onError: (message: String) -> Unit
    ) {
        when(val resource = dataSource.updateMeal(meal)){
            is ResourceWrapper.OnSuccess -> onSuccess(resource.data!!)
            is ResourceWrapper.OnError -> onError(resource.message!!)
        }
    }

    override suspend fun deleteMeal(
        id: Int,
        onComplete: (Unit) -> Unit,
        onError: (message: String) -> Unit
    ) {
        when(val resource = dataSource.deleteMeal(id)){
            is ResourceWrapper.OnComplete -> onComplete(Unit)
            is ResourceWrapper.OnError -> onError(resource.message!!)
        }
    }

    override suspend fun getFoodItems(
        query: String,
        onSuccess: (List<FoodItem>) -> Unit,
        onError: (message: String) -> Unit
    ) {
        when(val resource = dataSource.getFoodItems(query)){
            is ResourceWrapper.OnSuccess -> onSuccess(resource.data!!)
            is ResourceWrapper.OnError -> onError(resource.message!!)
        }
    }

    override suspend fun getNutritionInfo(
        query: String,
        onSuccess: (FoodItem) -> Unit,
        onError: (message: String) -> Unit
    ) {
        when(val resource = dataSource.getNutritionInfo(query)){
            is ResourceWrapper.OnSuccess -> onSuccess(resource.data!!)
            is ResourceWrapper.OnError -> onError(resource.message!!)
        }
    }
}