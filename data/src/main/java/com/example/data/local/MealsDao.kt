package com.example.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * Created by Andronicus Kim on 10/1/21.
 */
@Dao
interface MealsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUpdateMeal(mealsEntity: MealsEntity) : Long

    @Query("SELECT * FROM meals WHERE id=:id")
    suspend fun getMeal(id: Int) : MealsEntity

    @Query("SELECT * FROM meals WHERE date=:date")
    suspend fun getMeals(date: String) : List<MealsEntity>

    @Query("DELETE FROM meals WHERE id=:id")
    suspend fun deleteMeal(id: Int)
}