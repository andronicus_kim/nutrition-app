package com.example.data.local

import android.content.Context
import androidx.room.Room
import com.example.common.DATABASE_NAME

/**
 * Created by Andronicus Kim on 10/1/21.
 */
class RoomDatabaseInstance {
    companion object{
        fun getMealsDao(context: Context) = Room.databaseBuilder(context, MealsDatabase::class.java, DATABASE_NAME).build().getMealsDao()
    }
}