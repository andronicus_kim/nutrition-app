package com.example.data.local

import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.domain.models.Meal

/**
 * Created by Andronicus Kim on 10/1/21.
 */
@Entity(
    tableName = "meals"
)
@Keep
data class MealsEntity(
    @PrimaryKey(autoGenerate = true)
    var id : Int? = null,
    val date : String,
    val time: String,
    var name: String,
    val foodItems: FoodItems,
    val calories: Int
)

fun MealsEntity.toMeal(): Meal {
    return Meal(
        id = id,
        date = date,
        time = time,
        name = name,
        foodItems = foodItems.foodItems,
        calories = calories
    )
}

fun Meal.toMealEntity(): MealsEntity{
    return MealsEntity(
        id = id,
        date = date,
        time = time,
        name = name,
        foodItems = FoodItems(foodItems),
        calories = calories
    )
}