package com.example.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

/**
 * Created by Andronicus Kim on 10/1/21.
 */
@Database(
    entities = [MealsEntity::class],
    version = 1
)
@TypeConverters(Converters::class)
abstract class MealsDatabase : RoomDatabase(){
    abstract fun getMealsDao() : MealsDao
}