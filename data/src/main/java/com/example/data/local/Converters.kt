package com.example.data.local

import androidx.room.TypeConverter
import com.google.gson.Gson

/**
 * Created by Andronicus Kim on 10/1/21.
 */
class Converters {

    @TypeConverter
    fun fromFoodItems(foodItems: FoodItems) : String{
        return Gson().toJson(foodItems)
    }

    @TypeConverter
    fun toFoodItems(jsonString : String) : FoodItems{
        return Gson().fromJson(jsonString,FoodItems::class.java)
    }
}