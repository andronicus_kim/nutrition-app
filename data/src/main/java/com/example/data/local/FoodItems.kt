package com.example.data.local

import androidx.annotation.Keep
import com.example.domain.models.FoodItem

/**
 * Created by Andronicus Kim on 10/1/21.
 */
@Keep
data class FoodItems(val foodItems: List<FoodItem>)