package com.example.data.remote.dto.response

import androidx.annotation.Keep
import com.example.domain.models.FoodItem
import com.google.gson.annotations.SerializedName

/**
 * Created by Andronicus Kim on 10/1/21.
 */
@Keep
data class FoodItemsResponse(
    @SerializedName("common")
    val common: List<Common>
) {
    @Keep
    data class Common(
        @SerializedName("food_name")
        val foodName: String
    )
}

fun FoodItemsResponse.toFoodItems(): List<FoodItem>{
    return this.common.map {
        FoodItem(
            photoUrl = "",
            name = it.foodName,
            calories = 0,
            servings = 0
        )
    }
}