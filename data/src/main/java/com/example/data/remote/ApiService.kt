package com.example.data.remote

import com.example.data.remote.dto.request.NutritionInfoRequest
import com.example.data.remote.dto.response.FoodItemsResponse
import com.example.data.remote.dto.response.NutritionInfoResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by Andronicus Kim on 10/1/21.
 */
interface ApiService {

    @GET("/v2/search/instant")
    suspend fun getFoodItems(
        @Query("query") query : String
    ) : Response<FoodItemsResponse>

    @POST("/v2/natural/nutrients")
    suspend fun getNutritionInfo(
        @Body request: NutritionInfoRequest
    ) : Response<NutritionInfoResponse>
}