package com.example.data.remote

import android.content.Context
import com.example.common.BASE_URL
import com.example.common.NetworkCallInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Andronicus Kim on 10/1/21.
 */
class RetrofitInstance {
    companion object{
        fun getApiService(context: Context, isDebug: Boolean, headers: Map<String,String>) : ApiService {
            val loggingInterceptor = HttpLoggingInterceptor()
            if (isDebug){
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            }
            val client = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(NetworkCallInterceptor(context, headers))
                .retryOnConnectionFailure(true)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(ApiService::class.java)
        }
    }
}