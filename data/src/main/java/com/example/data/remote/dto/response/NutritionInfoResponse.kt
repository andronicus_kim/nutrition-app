package com.example.data.remote.dto.response

import androidx.annotation.Keep
import com.example.domain.models.FoodItem
import com.google.gson.annotations.SerializedName

/**
 * Created by Andronicus Kim on 10/1/21.
 */
@Keep
data class NutritionInfoResponse(
    @SerializedName("foods")
    val foods: List<Food>
) {
    @Keep
    data class Food(
        @SerializedName("food_name")
        val foodName: String,
        @SerializedName("nf_calories")
        val nfCalories: Double,
        @SerializedName("photo")
        val photo: Photo,
        @SerializedName("serving_qty")
        val servingQty: Double,
    ) {

        @Keep
        data class Photo(
            @SerializedName("thumb")
            val thumb: String
        )
    }
}

fun NutritionInfoResponse.toFoodItem(): FoodItem {
    val food = this.foods[0]
    return FoodItem(
        photoUrl =  food.photo.thumb,
        name = food.foodName,
        calories = food.nfCalories.toInt(),
        servings = food.servingQty.toInt()
    )
}