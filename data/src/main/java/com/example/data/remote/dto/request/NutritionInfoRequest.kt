package com.example.data.remote.dto.request

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

/**
 * Created by Andronicus Kim on 10/1/21.
 */
@Keep
data class NutritionInfoRequest(
    @SerializedName("query")
    val query: String
)