package com.example.data

import com.example.common.NoNetworkException
import com.example.data.local.MealsDao
import com.example.data.local.toMeal
import com.example.data.local.toMealEntity
import com.example.data.remote.ApiService
import com.example.data.remote.dto.request.NutritionInfoRequest
import com.example.data.remote.dto.response.toFoodItem
import com.example.data.remote.dto.response.toFoodItems
import com.example.domain.DataSource
import com.example.domain.ResourceWrapper
import com.example.domain.models.FoodItem
import com.example.domain.models.Meal
import java.io.IOException
import javax.inject.Inject

/**
 * Created by Andronicus Kim on 10/1/21.
 */
class DataSourceImpl @Inject constructor(
    private val dao: MealsDao,
    private val apiService: ApiService
) : DataSource {

    override suspend fun addMeal(meal: Meal): ResourceWrapper<Meal> {
        return try {
            dao.addUpdateMeal(meal.toMealEntity())
            ResourceWrapper.OnSuccess(meal)
        }catch (e : Exception){
            ResourceWrapper.OnError(e.message?:"Error adding meal")
        }
    }

    override suspend fun getMeal(id: Int): ResourceWrapper<Meal> {
        return try {
            val meal = dao.getMeal(id).toMeal()
            ResourceWrapper.OnSuccess(meal)
        }catch (e : Exception){
            ResourceWrapper.OnError(e.message?:"Error getting meal")
        }
    }

    override suspend fun getMeals(date: String): ResourceWrapper<List<Meal>> {
        return try {
            val meals = dao.getMeals(date).map { it.toMeal() }
            ResourceWrapper.OnSuccess(meals)
        }catch (e: Exception){
            ResourceWrapper.OnError(e.message?:"Error getting meals")
        }
    }

    override suspend fun updateMeal(meal: Meal): ResourceWrapper<Meal> {
        return try {
            dao.addUpdateMeal(meal.toMealEntity())
            ResourceWrapper.OnSuccess(meal)
        }catch (e : Exception){
            ResourceWrapper.OnError(e.message?:"Error updating meal")
        }
    }

    override suspend fun deleteMeal(id: Int): ResourceWrapper<Meal> {
        return try {
            dao.deleteMeal(id)
            ResourceWrapper.OnComplete()
        }catch (e : Exception){
            ResourceWrapper.OnError(e.message?:"Error deleting meal")
        }
    }

    override suspend fun getFoodItems(query: String): ResourceWrapper<List<FoodItem>> {
        try {
            val response = apiService.getFoodItems(query)
            if (response.isSuccessful){
                response.body()?.let {
                    return ResourceWrapper.OnSuccess(it.toFoodItems())
                }?: return ResourceWrapper.OnError("An error occurred!")
            }else{
                return ResourceWrapper.OnError(response.message())
            }
        }catch (e : Exception){
            return when(e){
                is NoNetworkException -> ResourceWrapper.OnError("No internet connection!")
                is IOException -> ResourceWrapper.OnError("Network error!")
                else -> ResourceWrapper.OnError("An error occurred!")
            }
        }
    }

    override suspend fun getNutritionInfo(query: String): ResourceWrapper<FoodItem> {
        try {
            val response = apiService.getNutritionInfo(NutritionInfoRequest(query))
            if (response.isSuccessful){
                response.body()?.let {
                    return ResourceWrapper.OnSuccess(it.toFoodItem())
                }?: return ResourceWrapper.OnError("An error occurred!")
            }else{
                return ResourceWrapper.OnError(response.message())
            }
        }catch (e : Exception){
            return when(e){
                is NoNetworkException -> ResourceWrapper.OnError("No internet connection!")
                is IOException -> ResourceWrapper.OnError("Network error!")
                else -> ResourceWrapper.OnError("An error occurred!")
            }
        }
    }
}