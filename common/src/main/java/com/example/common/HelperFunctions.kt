package com.example.common

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Andronicus Kim on 10/1/21.
 */
fun showDatePicker(calendar: Calendar, context: Context, onDateSet: (String) -> Unit){
    val year = calendar.get(Calendar.YEAR)
    val month = calendar.get(Calendar.MONTH)
    val day = calendar.get(Calendar.DAY_OF_MONTH)

    DatePickerDialog(
        context,
        { _, y, m, d -> onDateSet(getFormattedDate(calendar.apply { set(y,m,d) })) },year,month,day
    ).show()
}

fun showTimePicker(calendar: Calendar, context: Context, onTimeSet: (String) -> Unit){
    val hour = calendar.get(Calendar.HOUR_OF_DAY)
    val minute = calendar.get(Calendar.MINUTE)

    TimePickerDialog(
        context,
        { _, h, m -> onTimeSet(String.format("%02d:%02d",h,m)) },
        hour,minute,false
    ).show()
}

fun getFormattedDate(calendar: Calendar): String = SimpleDateFormat("dd-M-yyyy").format(calendar.time)