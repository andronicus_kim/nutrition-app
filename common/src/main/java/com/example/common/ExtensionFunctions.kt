package com.example.common

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

/**
 * Created by Andronicus Kim on 10/1/21.
 */
fun Activity.showToast(message : String){
    Toast.makeText(
        this,
        message,
        Toast.LENGTH_LONG
    ).show()
}

fun AlertDialog.Builder.showDialog(title: String, view: View, onDoneClick: () -> Unit){
    if (view.parent != null){ (view.parent as ViewGroup).removeView(view) }
    this.apply {
        setTitle(title)
        setView(view)
        setPositiveButton("DONE") { d, _ ->
            onDoneClick()
            d.dismiss()
        }
        setNegativeButton("CANCEL") { d, _ -> d.dismiss() }
        setCancelable(false)
        create()
    }.show()
}