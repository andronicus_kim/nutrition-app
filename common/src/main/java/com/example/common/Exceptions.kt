package com.example.common

import java.io.IOException

/**
 * Created by Andronicus Kim on 10/1/21.
 */
class NoNetworkException : IOException()