package com.example.common

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by Andronicus Kim on 10/1/21.
 */
//Intercepts requests, checks for connectivity and adds headers
class NetworkCallInterceptor(context: Context,private val map: Map<String,String>) : Interceptor {
    private val appContext = context.applicationContext
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!hasInternetConnection()) throw NoNetworkException()
        val request = chain.request().newBuilder().apply {
            map.entries.forEach { header ->
                addHeader(header.key,header.value)
            }
        }.build()
        return chain.proceed(request)
    }

    private fun hasInternetConnection(): Boolean {
        val connectivityManager = appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetwork ?: return false
        val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
        return when {
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }
}