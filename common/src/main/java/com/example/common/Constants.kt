package com.example.common

/**
 * Created by Andronicus Kim on 10/1/21.
 */
const val BASE_URL = "https://trackapi.nutritionix.com"
const val DATABASE_NAME = "meals_db.db"
const val APP_ID = "x-app-id"
const val APP_KEY = "x-app-key"
const val BARCODE_SCANNER_RESULT = "barcode_scanner_result"