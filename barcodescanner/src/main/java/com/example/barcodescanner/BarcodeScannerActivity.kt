package com.example.barcodescanner

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.example.common.BARCODE_SCANNER_RESULT
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView

/**
 * Created by Andronicus Kim on 10/1/21.
 */
class BarcodeScannerActivity: AppCompatActivity(), ZXingScannerView.ResultHandler {
    private lateinit var barcodeScanner: ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.requestFeature(Window.FEATURE_ACTION_BAR)
        window.setFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS,0)
        window.statusBarColor = Color.WHITE
        supportActionBar?.hide()

        barcodeScanner = ZXingScannerView(this)
        setContentView(barcodeScanner)
    }

    public override fun onResume() {
        super.onResume()
        barcodeScanner.setResultHandler(this) // Register ourselves as a handler for scan results.
        barcodeScanner.startCamera()          // Start camera in on resume
    }

    public override fun onPause() {
        super.onPause()
        barcodeScanner.stopCamera()           // Stop camera in on pause
    }

    override fun handleResult(result: Result?) {
        Intent().apply {
            putExtra(BARCODE_SCANNER_RESULT, result?.text)
            setResult(RESULT_OK,this)
        }
        finish()
    }
}