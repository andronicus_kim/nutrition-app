package com.example.nutritionapp.data

import com.example.domain.Repository
import com.example.domain.models.FoodItem
import com.example.domain.models.Meal

/**
 * Created by Andronicus Kim on 9/22/21.
 */
class FakeRepositoryImpl: Repository {

    //Sample data
    private val testMeal = Meal(null, "","","", listOf(),0)
    private val testMeals = listOf<Meal>()
    private val testFoodItems = listOf<FoodItem>()
    private val testFoodItem = FoodItem("","",0,0)
    private val errorMessage = "error"
    private var shouldReturnError = false

    //Helper function to set the result we want to be returned
    fun setShouldReturnError(value: Boolean) {
        shouldReturnError = value
    }


    override suspend fun addMeal(
        meal: Meal,
        onSuccess: (meal: Meal) -> Unit,
        onError: (message: String) -> Unit
    ) {
        if (shouldReturnError){
            onError(errorMessage)
        }else{
            onSuccess(testMeal)
        }
    }

    override suspend fun getMeal(
        id: Int,
        onSuccess: (meal: Meal) -> Unit,
        onError: (message: String) -> Unit
    ) {
        if (shouldReturnError){
            onError(errorMessage)
        }else{
            onSuccess(testMeal)
        }
    }

    override suspend fun getMeals(
        date: String,
        onSuccess: (meals: List<Meal>) -> Unit,
        onError: (message: String) -> Unit
    ) {
        if (shouldReturnError){
            onError(errorMessage)
        }else{
            onSuccess(testMeals)
        }
    }

    override suspend fun updateMeal(
        meal: Meal,
        onSuccess: (meal: Meal) -> Unit,
        onError: (message: String) -> Unit
    ) {
        if (shouldReturnError){
            onError(errorMessage)
        }else{
            onSuccess(testMeal)
        }
    }

    override suspend fun deleteMeal(
        id: Int,
        onComplete: (Unit) -> Unit,
        onError: (message: String) -> Unit
    ) {
        if (shouldReturnError){
            onError(errorMessage)
        }else{
            onComplete(Unit)
        }
    }

    override suspend fun getFoodItems(
        query: String,
        onSuccess: (List<FoodItem>) -> Unit,
        onError: (message: String) -> Unit
    ) {
        if (shouldReturnError){
            onError(errorMessage)
        }else{
            onSuccess(testFoodItems)
        }
    }

    override suspend fun getNutritionInfo(
        query: String,
        onSuccess: (FoodItem) -> Unit,
        onError: (message: String) -> Unit
    ) {
        if (shouldReturnError){
            onError(errorMessage)
        }else{
            onSuccess(testFoodItem)
        }
    }
}