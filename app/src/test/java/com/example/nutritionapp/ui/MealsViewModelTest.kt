package com.example.nutritionapp.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.domain.models.Meal
import com.example.nutritionapp.MainCoroutineRule
import com.example.nutritionapp.data.FakeRepositoryImpl
import com.example.nutritionapp.getOrAwaitValueTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Created by Andronicus Kim on 9/22/21.
 */
class MealsViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var repository: FakeRepositoryImpl
    private lateinit var viewModel: MealsViewModel

    @Before
    fun setUp() {
        repository = FakeRepositoryImpl()
        viewModel = MealsViewModel(repository)
    }

    @Test
    fun `network call to get food items returns success`(){
        val query = "food item"
        viewModel.getFoodItems(query)

        val result = viewModel.getFoodItems.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `network call to get food items returns error`(){
        repository.setShouldReturnError(true)

        val query = "food item"
        viewModel.getFoodItems(query)

        val result = viewModel.errorMessage.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `network call to get nutrition info returns success`(){
        viewModel.getNutritionInfo("")

        val result = viewModel.getNutritionInfo.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `network call to get nutrition info returns error`(){
        repository.setShouldReturnError(true)

        viewModel.getNutritionInfo("")

        val result = viewModel.errorMessage.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `add meal returns success`(){
        val meal = Meal(null, "","","", listOf(),0)
        viewModel.addMeal(meal)

        val result = viewModel.addMeal.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `add meal returns error`(){
        repository.setShouldReturnError(true)

        val meal = Meal(null, "","","", listOf(),0)
        viewModel.addMeal(meal)

        val result = viewModel.errorMessage.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `get meal returns success`(){
        viewModel.getMeal(0)

        val result = viewModel.getMeal.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `get meal returns error`(){
        repository.setShouldReturnError(true)

        viewModel.getMeal(0)

        val result = viewModel.errorMessage.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `get meals returns success`(){
        viewModel.getMeals("")

        val result = viewModel.getMeals.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `get meals returns error`(){
        repository.setShouldReturnError(true)

        viewModel.getMeals("")

        val result = viewModel.errorMessage.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `update meal returns success`(){
        val meal = Meal(null, "","","", listOf(),0)
        viewModel.updateMeal(meal)

        val result = viewModel.updateMeal.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `update meal returns error`(){
        repository.setShouldReturnError(true)

        val meal = Meal(null, "","","", listOf(),0)
        viewModel.updateMeal(meal)

        val result = viewModel.errorMessage.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `delete meal returns success`(){
        viewModel.deleteMeal(0)

        val result = viewModel.deleteMeal.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }

    @Test
    fun `delete meal returns error`(){
        repository.setShouldReturnError(true)

        viewModel.deleteMeal(0)

        val result = viewModel.errorMessage.getOrAwaitValueTest()

        assertThat(result).isNotNull()
    }
}