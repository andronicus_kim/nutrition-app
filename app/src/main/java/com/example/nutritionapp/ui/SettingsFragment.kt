package com.example.nutritionapp.ui

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.example.nutritionapp.R

/**
 * Created by Andronicus Kim on 9/23/21.
 */
class SettingsFragment: PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences,rootKey)
    }
}