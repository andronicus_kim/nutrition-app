package com.example.nutritionapp.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.nutritionapp.R

/**
 * Created by Andronicus Kim on 9/23/21.
 */
class SettingsActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings_container, SettingsFragment())
            .commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        //Refresh data
        startActivity(Intent(this,MainActivity::class.java))
        finishAffinity()
    }
}