package com.example.nutritionapp.ui.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.models.Meal
import com.example.nutritionapp.R
import com.example.nutritionapp.ui.MealDetailsActivity

/**
 * Created by Andronicus Kim on 9/24/21.
 */
class MealsAdapter(private val list: List<Meal>) : RecyclerView.Adapter<MealsAdapter.MealsViewHolder>(){

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealsViewHolder {
        context = parent.context
        return MealsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.meal_list_item,parent,false))
    }

    override fun onBindViewHolder(holder: MealsViewHolder, position: Int) {
        val meal = list[position]
        val calories = "${meal.calories} cal"
        holder.apply {
            mealName.text = meal.name
            mealCalories.text = calories
            itemView.setOnClickListener {
                context.startActivity(
                    Intent(context, MealDetailsActivity::class.java)
                    .apply { putExtra("meal_id",meal.id) })
            }
            setBackgroundColor(root,position)
        }
    }

    private fun setBackgroundColor(view: View, position: Int){
        when(position){
            0 -> view.setBackgroundColor(context.resources.getColor(R.color.purple_700))
            1 -> view.setBackgroundColor(context.resources.getColor(R.color.black))
            2 -> view.setBackgroundColor(context.resources.getColor(R.color.design_default_color_error))
            3 -> view.setBackgroundColor(context.resources.getColor(R.color.teal_700))
            4 -> view.setBackgroundColor(context.resources.getColor(R.color.purple_200))
            else -> view.setBackgroundColor(context.resources.getColor(R.color.purple_700))
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MealsViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val mealName: TextView = view.findViewById(R.id.tv_meal_name)
        val mealCalories: TextView = view.findViewById(R.id.tv_meal_calories)
        val root: LinearLayout = view.findViewById(R.id.ll_root_view)
    }
}