package com.example.nutritionapp.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.common.showDialog
import com.example.domain.models.FoodItem
import com.example.nutritionapp.R

/**
 * Created by Andronicus Kim on 9/24/21.
 */
class FoodItemsAdapter(
    private val list: MutableList<FoodItem>,
    private val isDetailView: Boolean,
    private val listener: SwipeToDeleteListener
) : RecyclerView.Adapter<FoodItemsAdapter.FoodItemsViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodItemsViewHolder {
        context = parent.context
        return FoodItemsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.food_item_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: FoodItemsViewHolder, position: Int) {
        val foodItem = list[position]
        val caloriesText = "${foodItem.calories} calories per serving"
        val servingsText = "${foodItem.servings} serving(s)"
        holder.apply {

            Glide.with(context)
                .load(foodItem.photoUrl)
                .centerCrop()
                .placeholder(R.drawable.food_item_placeholder)
                .dontAnimate()
                .into(photo)

            name.text = foodItem.name.lowercase()
            calories.text = caloriesText
            servings.text = servingsText
            itemView.setOnClickListener {
                if (isDetailView) return@setOnClickListener
                editFoodItem(foodItem)
            }
        }
    }

    private fun editFoodItem(foodItem: FoodItem){
        val foodItemLayout = LayoutInflater.from(context).inflate(R.layout.food_item_input, null)
        val fooItemName: EditText = foodItemLayout.findViewById(R.id.et_food_item_name)
        val fooItemCalories: EditText = foodItemLayout.findViewById(R.id.et_food_item_calories)
        val fooItemServings: EditText = foodItemLayout.findViewById(R.id.et_food_item_servings)

        //Set current food item data
        fooItemName.setText(foodItem.name)
        fooItemCalories.setText(foodItem.calories.toString())
        fooItemServings.setText(foodItem.servings.toString())
        AlertDialog.Builder(context).showDialog(
            "Edit Food Item",
            foodItemLayout
        ) {
            //Update food item
            foodItem.name = fooItemName.text.toString()
            foodItem.calories = fooItemCalories.text.toString().toInt()
            foodItem.servings = fooItemServings.text.toString().toInt()
            notifyDataSetChanged()
        }
    }

    //A listener for when a user swipes LEFT to delete an item
    interface SwipeToDeleteListener{
        fun onItemDeleted(position: Int, foodItem: FoodItem)
    }
    fun addItem(foodItem: FoodItem) {
        list.add(foodItem)
        notifyDataSetChanged()
    }

    fun refreshData(foodItems: List<FoodItem>) {
        list.clear()
        list.addAll(foodItems)
        notifyDataSetChanged()
    }

    fun deleteItem(position: Int){
        //Delete item and notify listener
        val foodItem = list[position]
        list.remove(foodItem)
        notifyItemRemoved(position)
        notifyDataSetChanged()
        listener.onItemDeleted(position, foodItem)
    }

    fun getAllData() = list

    override fun getItemCount(): Int {
        return list.size
    }

    inner class FoodItemsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val photo: ImageView = view.findViewById(R.id.iv_photo)
        val name: TextView = view.findViewById(R.id.tv_name)
        val calories: TextView = view.findViewById(R.id.tv_calories)
        val servings: TextView = view.findViewById(R.id.tv_servings)
    }

    class SwipeToDelete(private val adapter: FoodItemsAdapter): ItemTouchHelper.SimpleCallback(0,
        ItemTouchHelper.RIGHT){

        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean = false

        override fun onSwiped(holder: RecyclerView.ViewHolder, direction: Int) {
            adapter.deleteItem(holder.adapterPosition)
        }
    }
}