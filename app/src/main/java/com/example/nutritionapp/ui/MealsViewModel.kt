package com.example.nutritionapp.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.Repository
import com.example.domain.models.FoodItem
import com.example.domain.models.Meal
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Andronicus Kim on 9/22/21.
 */
@HiltViewModel
class MealsViewModel @Inject constructor(private val repository: Repository): ViewModel() {

    //LiveData Objects that we will expose to the view to observe for changes
    private val _addMeal = MutableLiveData<Meal>()
    val addMeal: LiveData<Meal>
        get() = _addMeal

    private val _getMeal = MutableLiveData<Meal>()
    val getMeal: LiveData<Meal>
        get() = _getMeal

    private val _getMeals = MutableLiveData<List<Meal>>()
    val getMeals: LiveData<List<Meal>>
        get() = _getMeals

    private val _updateMeal = MutableLiveData<Meal>()
    val updateMeal: LiveData<Meal>
        get() = _updateMeal

    private val _deleteMeal = MutableLiveData<Unit>()
    val deleteMeal: LiveData<Unit>
        get() = _deleteMeal

    private val _getFoodItems = MutableLiveData<List<FoodItem>>()
    val getFoodItems: LiveData<List<FoodItem>>
        get() = _getFoodItems

    private val _getNutritionInfo = MutableLiveData<FoodItem>()
    val getNutritionInfo: LiveData<FoodItem>
        get() = _getNutritionInfo

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    /*
    * Function implementations that in turn update our MutableLiveData objects
    * depending on the the result we get from the repository
    * */
    fun addMeal(meal: Meal) = viewModelScope.launch { repository.addMeal(meal, _addMeal::postValue, _errorMessage::postValue) }

    fun getMeal(id: Int) = viewModelScope.launch { repository.getMeal(id,_getMeal::postValue, _errorMessage::postValue) }

    fun getMeals(date: String) = viewModelScope.launch { repository.getMeals(date, _getMeals::postValue, _errorMessage::postValue) }

    fun updateMeal(meal: Meal) = viewModelScope.launch { repository.updateMeal(meal, _updateMeal::postValue, _errorMessage::postValue) }

    fun deleteMeal(id: Int) = viewModelScope.launch { repository.deleteMeal(id, _deleteMeal::postValue, _errorMessage::postValue) }

    fun getFoodItems(query: String)= viewModelScope.launch { repository.getFoodItems(query, _getFoodItems::postValue, _errorMessage::postValue) }

    fun getNutritionInfo(query: String) = viewModelScope.launch { repository.getNutritionInfo(query, _getNutritionInfo::postValue, _errorMessage::postValue) }
}