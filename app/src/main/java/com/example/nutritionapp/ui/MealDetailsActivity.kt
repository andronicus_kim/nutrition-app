package com.example.nutritionapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.common.showToast
import com.example.domain.models.FoodItem
import com.example.domain.models.Meal
import com.example.nutritionapp.R
import com.example.nutritionapp.databinding.ActivityMealDetailsBinding
import com.example.nutritionapp.ui.adapters.FoodItemsAdapter
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by Andronicus Kim on 9/24/21.
 */
@AndroidEntryPoint
class MealDetailsActivity: AppCompatActivity(), FoodItemsAdapter.SwipeToDeleteListener {

    private val viewModel: MealsViewModel by viewModels()
    private lateinit var binding: ActivityMealDetailsBinding
    private lateinit var adapter: FoodItemsAdapter
    private var mealId: Int?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMealDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        adapter = FoodItemsAdapter(mutableListOf(),true,this)

        val recyclerView: RecyclerView = findViewById(R.id.rv_meal_food_items)
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)
        recyclerView.adapter = adapter

        mealId = intent.getIntExtra("meal_id",0)
        //use the passed in meal id to get a meal from storage
        viewModel.getMeal(mealId!!)

        bindUI()
    }

    private fun bindUI(){
        viewModel.errorMessage.observe(this,this::showToast)
        viewModel.getMeal.observe(this, this::showMealDetails)
        viewModel.deleteMeal.observe(this,{
            showToast("Deleted successfully!")
            startActivity(Intent(this,MainActivity::class.java))
            finishAffinity()
        })
    }

    override fun onItemDeleted(position: Int, foodItem: FoodItem) {

    }

    private fun showMealDetails(meal: Meal){
        val date = "${meal.date} ${meal.time}"
        binding.tvMealDate.text = date
        binding.tvMealName.text = meal.name
        adapter.refreshData(meal.foodItems)
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.meal_details,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.ic_edit -> {
                startActivity(Intent(this,AddEditMealActivity::class.java).apply {
                    //Meal is being edited so is_edit is true
                    putExtra("is_edit",true)
                    putExtra("meal_id", mealId)
                })
                true
            }
            R.id.ic_delete -> {
                viewModel.deleteMeal(mealId!!)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }
}