package com.example.nutritionapp.ui

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.GridLayoutManager
import com.example.common.getFormattedDate
import com.example.common.showToast
import com.example.nutritionapp.R
import com.example.nutritionapp.databinding.ActivityMainBinding
import com.example.nutritionapp.ui.adapters.MealsAdapter
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MealsViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.requestFeature(Window.FEATURE_ACTION_BAR)
        window.setFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS,0)
        window.statusBarColor = Color.WHITE
        supportActionBar?.hide()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val calendar = Calendar.getInstance()

        binding.apply {
            ivPrevious.setOnClickListener {
                calendar.add(Calendar.DAY_OF_MONTH, -1)
                getMeals(calendar)
            }
            ivNext.setOnClickListener {
                calendar.add(Calendar.DAY_OF_MONTH, 1)
                getMeals(calendar)
            }
            bottomAppBar.setOnMenuItemClickListener { menuItem ->
                if (menuItem.itemId == R.id.action_settings){
                    startActivity(Intent(this@MainActivity,SettingsActivity::class.java))
                    true
                }else{
                    false
                }
            }
            fab.setOnClickListener {
                startActivity(
                    Intent(this@MainActivity,AddEditMealActivity::class.java)
                    .apply { putExtra("is_edit",false) })
            }
            rvMeals.layoutManager = GridLayoutManager(this@MainActivity,2)
        }

        getMeals(calendar)

        bindUI()
    }

    private fun bindUI(){
        viewModel.errorMessage.observe(this,this::showToast)
        viewModel.getMeals.observe(this){ meals ->
            binding.llPlaceholder.apply { visibility = if (meals.isEmpty()) View.VISIBLE else View.GONE }
            binding.rvMeals.adapter = MealsAdapter(meals)
            val consumedCalories = meals.sumOf { it.calories }
            showRemainingCalories(consumedCalories)
        }
    }

    private fun getMeals(calendar: Calendar){
        val chosenDate = getFormattedDate(calendar)

        viewModel.getMeals(chosenDate)

        val yesterday = getFormattedDate(Calendar.getInstance().apply { add(Calendar.DAY_OF_MONTH, -1) })
        val today = getFormattedDate(Calendar.getInstance())
        val tomorrow = getFormattedDate(Calendar.getInstance().apply { add(Calendar.DAY_OF_MONTH, 1) })
        binding.tvToday.apply {
            text = when(chosenDate){
                yesterday -> "Yesterday"
                today -> "Today"
                tomorrow -> "Tomorrow"
                else -> chosenDate
            }
        }
    }

    private fun showRemainingCalories(consumedCalories: Int){
        //get calories target from shared preferences and perform calculations
        val prefs = PreferenceManager.getDefaultSharedPreferences(this).getString("calories_target","")
        val caloriesTarget = if (prefs.isNullOrEmpty() || prefs.contentEquals("0")) 0 else prefs.toInt()
        if (caloriesTarget == 0){
            binding.tvPercentage.text = "100%"
            binding.tvRemainingCalories.visibility = View.GONE
        }else{
            if (consumedCalories > caloriesTarget){
                //calories target has been exceeded
                binding.tvPercentage.text = "0%"
                binding.tvRemainingCalories.text = "0"
                binding.tvRemainingCalories.visibility = View.VISIBLE
            }else{
                val remainingCalories = caloriesTarget - consumedCalories
                val percentage = ((remainingCalories.toDouble())/caloriesTarget) * 100
                val percentageText = "${percentage.toInt()}%"
                binding.tvPercentage.text = percentageText
                binding.tvRemainingCalories.text = remainingCalories.toString()
                binding.tvRemainingCalories.visibility = View.VISIBLE
            }
        }
    }
}