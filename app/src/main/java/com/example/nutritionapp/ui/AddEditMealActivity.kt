package com.example.nutritionapp.ui

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.barcodescanner.BarcodeScannerActivity
import com.example.common.*
import com.example.domain.models.FoodItem
import com.example.domain.models.Meal
import com.example.nutritionapp.R
import com.example.nutritionapp.databinding.ActivityAddEditMealBinding
import com.example.nutritionapp.ui.adapters.FoodItemsAdapter
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

/**
 * Created by Andronicus Kim on 9/24/21.
 */
@AndroidEntryPoint
class AddEditMealActivity : AppCompatActivity(), FoodItemsAdapter.SwipeToDeleteListener {

    private var date = ""
    private var time = ""
    private var name = ""
    private var mealId: Int? = null
    private lateinit var progressDialog: ProgressDialog
    private lateinit var foodItemsAdapter: FoodItemsAdapter
    private val viewModel: MealsViewModel by viewModels()
    private lateinit var binding: ActivityAddEditMealBinding
    private lateinit var autoComplete: AutoCompleteTextView
    private lateinit var dialog: AlertDialog
    private var foodItems = listOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddEditMealBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Set action bar title depending on the action being performed (Add/Edit)
        val isEdit = intent.getBooleanExtra("is_edit",false)
        supportActionBar?.title = if (isEdit){ "Edit Meal" } else { "Add Meal" }

        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Please wait...")

        foodItemsAdapter = FoodItemsAdapter(mutableListOf(),false,this)

        //Instantiate TouchHelper and attach it to recyclerview
        val touchHelper = ItemTouchHelper(FoodItemsAdapter.SwipeToDelete(foodItemsAdapter))
        binding.rvFoodItems.apply {
            layoutManager = LinearLayoutManager(this@AddEditMealActivity,
                RecyclerView.VERTICAL,false)
            adapter = foodItemsAdapter
            touchHelper.attachToRecyclerView(this)
        }

        binding.btnScanBarcode.setOnClickListener { requestPermission() }
        binding.btnSearchFromApi.setOnClickListener { searchFromApi() }
        binding.btnAddManually.setOnClickListener {
            val foodItemLayout = LayoutInflater.from(this).inflate(R.layout.food_item_input,null)
            val fooItemName: EditText = foodItemLayout.findViewById(R.id.et_food_item_name)
            val fooItemCalories: EditText = foodItemLayout.findViewById(R.id.et_food_item_calories)
            val fooItemServings: EditText = foodItemLayout.findViewById(R.id.et_food_item_servings)
            AlertDialog.Builder(this)
                .showDialog("Add Food Item",foodItemLayout) {
                    foodItemsAdapter.addItem(
                        FoodItem("",fooItemName.text.toString(), fooItemCalories.text.toString().toInt(),
                        fooItemServings.text.toString().toInt())
                    )
                }
        }

        binding.tvSetMealName.setOnClickListener {
            val mealNameInputLayout = LayoutInflater.from(this).inflate(R.layout.meal_name_input,null)
            AlertDialog.Builder(this)
                .showDialog("Enter Meal Name",mealNameInputLayout) {
                    name = mealNameInputLayout.findViewById<EditText>(R.id.et_meal_name).text.toString()
                    binding.tvSetMealName.text = name
                }
        }
        binding.tvSetDate.setOnClickListener {
            showDatePicker(Calendar.getInstance(), this) { date ->
                this.date = date
                showTimePicker(Calendar.getInstance(), this) { time ->
                    this.time = time
                    val dateTime = "$date $time"
                    binding.tvSetDate.text = dateTime
                }
            }
        }

        if (isEdit) viewModel.getMeal(intent.getIntExtra("meal_id",0))

        bindUI()
    }

    private fun bindUI(){
        viewModel.addMeal.observe(this, {
            showToast("Saved successfully!")
            startActivity(Intent(this,MainActivity::class.java))
            finishAffinity()
        })

        viewModel.getMeal.observe(this){ meal ->
            date = meal.date
            time = meal.time
            name = meal.name
            mealId = meal.id

            val dateText = "$date $time"
            binding.tvSetDate.text = dateText
            binding.tvSetMealName.text = meal.name
            foodItemsAdapter.refreshData(meal.foodItems)
        }
        viewModel.errorMessage.observe(this){
            dismissDialog()
            if(this::dialog.isInitialized) dialog.dismiss()
            showToast(it)
        }
        viewModel.getFoodItems.observe(this){ response->
            val list = response.map {  it.name }
            foodItems = list
            val arrayAdapter = ArrayAdapter(this@AddEditMealActivity,android.R.layout.simple_list_item_1,list)
            autoComplete.setAdapter(arrayAdapter)
        }
        viewModel.getNutritionInfo.observe(this){
            dismissDialog()
            foodItemsAdapter.addItem(it)
        }
    }

    private fun searchFromApi(){
        foodItems = listOf()
        val layout = LayoutInflater.from(this).inflate(R.layout.api_search_layout,null)
        autoComplete = layout.findViewById(R.id.tv_autocomplete)
        autoComplete.setOnItemClickListener { adapterView, _, position, _ ->
            //get selected item
            val foodItem = adapterView.getItemAtPosition(position) as String

            if(this::dialog.isInitialized) dialog.dismiss()

            showDialog()
            //get nutrition info
            viewModel.getNutritionInfo(foodItem)
        }
        autoComplete.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(query: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (query.toString().isEmpty()) foodItems = listOf()
                if (autoComplete.enoughToFilter()){
                    if (foodItems.isEmpty()){
                        //get food items
                        viewModel.getFoodItems(query.toString())
                    }
                }
            }

            override fun afterTextChanged(p0: Editable?) {

            }
        })

        if (layout.parent != null) (layout.parent as ViewGroup).removeView(layout)
        dialog = AlertDialog.Builder(this)
            .setTitle("Search Food Item")
            .setView(layout)
            .create()
        dialog.show()
    }

    override fun onItemDeleted(position: Int, foodItem: FoodItem) {
        Snackbar.make(
            binding.rvFoodItems,
            "Food Item deleted!",
            Snackbar.LENGTH_LONG
        ).setAction("Undo"){
            //Add the item that was deleted back into the list
            foodItemsAdapter.getAllData().add(position,foodItem)
            foodItemsAdapter::notifyItemInserted
            foodItemsAdapter.notifyDataSetChanged()
        }.show()
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add_meal, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.ic_done){
            val foodItems = foodItemsAdapter.getAllData()
            val calories = foodItems.sumOf { (it.calories) * (it.servings) }
            when {
                date.isEmpty() -> {
                    showToast("Date & Time is required!")
                }
                name.isEmpty() -> {
                    showToast("Meal Name is required!")
                }
                foodItems.isEmpty() -> {
                    showToast("You've not added food item(s)")
                }
                else -> {
                    viewModel.addMeal(Meal(mealId,date,time,name,foodItems,calories))
                }
            }
            true
        }else{
            super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //Handle the result we get back from Barcode scanning
        if (requestCode == 1 && resultCode == RESULT_OK){
            val foodItem = data?.getStringExtra(BARCODE_SCANNER_RESULT)?:""

            //get food items
            if (foodItem.isNotEmpty()) {
                showDialog()
                viewModel.getNutritionInfo(foodItem)
            }
        }
    }

    private fun requestPermission(){
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            //request permission from user if the app hasn't got the required permission
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA), //request specific permission from user
                10
            )
        } else {
            try {
                startActivityForResult(Intent(this, BarcodeScannerActivity::class.java),1)
            } catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(
                    this,
                    "No camera application installed",
                    Toast.LENGTH_SHORT
                ).show()
            }

        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            10 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(
                        this,
                        "Permission is needed in order to scan",
                        Toast.LENGTH_LONG
                    ).show();
                } else {
                    startActivityForResult(Intent(this,BarcodeScannerActivity::class.java),1)
                }
            }
        }
    }

    private fun showDialog() {
        if (!progressDialog.isShowing) {
            progressDialog.show()
        }
    }

    private fun dismissDialog() {
        if (progressDialog.isShowing) {
            progressDialog.dismiss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        dismissDialog()
    }
}