package com.example.nutritionapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Andronicus Kim on 9/22/21.
 */
@HiltAndroidApp
class NutritionApp: Application() {
}