package com.example.nutritionapp.di

import android.content.Context
import com.example.common.APP_ID
import com.example.common.APP_KEY
import com.example.data.local.MealsDao
import com.example.data.local.RoomDatabaseInstance
import com.example.data.remote.ApiService
import com.example.data.remote.RetrofitInstance
import com.example.nutritionapp.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Andronicus Kim on 9/22/21.
 */
@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    @Singleton
    @Provides
    fun provideApiService(@ApplicationContext context: Context) : ApiService {
        return RetrofitInstance.getApiService(
            context,
            BuildConfig.DEBUG,
            mapOf(Pair(APP_ID,BuildConfig.APP_ID), Pair(APP_KEY,BuildConfig.APP_KEY))
        )
    }

    @Singleton
    @Provides
    fun provideDao(@ApplicationContext context: Context) : MealsDao {
        return RoomDatabaseInstance.getMealsDao(context)
    }
}