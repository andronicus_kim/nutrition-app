package com.example.nutritionapp.di

import com.example.data.DataSourceImpl
import com.example.domain.DataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Andronicus Kim on 9/22/21.
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourceModule {

    @Singleton
    @Binds
    abstract fun bindDataSource(dataSourceImpl: DataSourceImpl) : DataSource
}