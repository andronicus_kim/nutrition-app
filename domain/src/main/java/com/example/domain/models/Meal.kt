package com.example.domain.models

/**
 * Created by Andronicus Kim on 9/21/21.
 */
data class Meal(
    var id : Int? = null,
    val date : String,
    val time: String,
    var name: String,
    val foodItems: List<FoodItem>,
    val calories: Int
)