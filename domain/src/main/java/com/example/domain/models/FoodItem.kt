package com.example.domain.models

/**
 * Created by Andronicus Kim on 9/21/21.
 */
data class FoodItem(
    var photoUrl: String,
    var name: String,
    var calories: Int,
    var servings: Int
)