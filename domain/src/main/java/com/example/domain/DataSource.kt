package com.example.domain

import com.example.domain.models.FoodItem
import com.example.domain.models.Meal

/**
 * Created by Andronicus Kim on 9/21/21.
 */
interface DataSource {
    suspend fun addMeal(meal : Meal) : ResourceWrapper<Meal>
    suspend fun getMeal(id : Int) : ResourceWrapper<Meal>
    suspend fun getMeals(date: String) : ResourceWrapper<List<Meal>>
    suspend fun updateMeal(meal : Meal) : ResourceWrapper<Meal>
    suspend fun deleteMeal(id: Int) : ResourceWrapper<Meal>
    suspend fun getFoodItems(query : String) : ResourceWrapper<List<FoodItem>>
    suspend fun getNutritionInfo(query : String) : ResourceWrapper<FoodItem>
}