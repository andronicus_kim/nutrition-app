package com.example.domain

/**
 * Created by Andronicus Kim on 9/21/21.
 */
//A wrapper class around any resource we receive from the datasource
sealed class ResourceWrapper<out T>(
    val status: Status,
    val data: T?=null,
    val message: String?=null
){
    class OnSuccess<out T>(data: T): ResourceWrapper<T>(Status.SUCCESS,data,null)
    class OnError<out T>(message: String): ResourceWrapper<T>(Status.ERROR,null,message)
    class OnComplete<out T>: ResourceWrapper<T>(Status.COMPLETE,null,null)
}

enum class Status{
    SUCCESS,
    ERROR,
    COMPLETE
}