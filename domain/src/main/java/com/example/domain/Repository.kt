package com.example.domain

import com.example.domain.models.FoodItem
import com.example.domain.models.Meal

/**
 * Created by Andronicus Kim on 9/21/21.
 */
interface Repository {

    suspend fun addMeal(
        meal: Meal,
        onSuccess: (meal: Meal) -> Unit,
        onError: (message: String) -> Unit
    )

    suspend fun getMeal(
        id: Int,
        onSuccess: (meal: Meal) -> Unit,
        onError: (message: String) -> Unit
    )

    suspend fun getMeals(
        date: String,
        onSuccess: (meals: List<Meal>) -> Unit,
        onError: (message: String) -> Unit
    )

    suspend fun updateMeal(
        meal: Meal,
        onSuccess: (meal: Meal) -> Unit,
        onError: (message: String) -> Unit
    )

    suspend fun deleteMeal(
        id: Int,
        onComplete: (Unit) -> Unit,
        onError: (message: String) -> Unit
    )

    suspend fun getFoodItems(
        query: String,
        onSuccess: (List<FoodItem>) -> Unit,
        onError: (message: String) -> Unit
    )

    suspend fun getNutritionInfo(
        query: String,
        onSuccess: (FoodItem) -> Unit,
        onError: (message: String) -> Unit
    )
}