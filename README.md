# Nutrition App
[![andronicus_kim](https://circleci.com/bb/andronicus_kim/nutrition-app.svg?style=shield)](https://bitbucket.org/andronicus_kim/nutrition-app)

## Home Screen

![Scheme](https://bitbucket.org/andronicus_kim/nutrition-app/raw/master/screenshots/third.jpg)

## Add Meal

![Scheme](https://bitbucket.org/andronicus_kim/nutrition-app/raw/master/screenshots/sixth.jpg)

## Meal Details

![Scheme](https://bitbucket.org/andronicus_kim/nutrition-app/raw/master/screenshots/second.jpg)